
import asyncio
import ujson
from tabulate import tabulate
from copy import deepcopy
import websockets
import zlib
# ***This file do not consider the checksum***
#CheckSum
# as the returned data shows every book iteration, there will be a checkwum message : \
#     [ CHAIN_ID, 'cs', CHECKSUM ]
# where CHECKSUM is a signed integer.
# According to the API files, it uses CRC-32 checksum:
# So we can use CRC-32 to create the checksum value with the data returned and compare it with the checksum returned by the Websocket.
# But I was confused how we can get the checksum value by myself. Hope you can give me some tips.

# Pairs which generate orderbook for.
PAIRS = [
    "tBTCUSD",
     "tETCBTC",
     'tSTORJBTC',
]

conf = {
    "event": "subscribe",
    "channel": "book",
}
global orderbooks
orderbooks = {
    pair: {}
    for pair in PAIRS
}

def build_book(res,pair):
    global orderbooks
    if len(res)<45:
        # String to json
        data = ujson.loads(res)
        # print(data)
        # Build orderbook
        if data =="hb":
            return

        # if data[1] == "cs":
        #     checksum=data[2]
        #     csdata=[]
        #     psnap = {}
        #     bidskeys=psnap['bid']
        #     askskeys=psnap['ask']
        #     bid={}
        #     ask={}
        #     mcnt=0
        #     for i in range(25):
        #         price1=bidskeys[i]
        #         pp1=bid[price1]
        #         csdata.append(pp1['price'],pp1['amount'])
        #         price2=askskeys[i]
        #         pp2=ask[price2]
        #         csdata.append(pp2['price'],-pp2['amount'])
        #
        #     csstr=csdata.join(':')
        #     b=csstr.encode('utf8')
        #     a=zlib.crc32(b.encode('uft8'))
        #     print(a)
        #     if  a != checksum:
        #         print('checksum failed')
        #         exit(-1)
        #     else:
        #         print('checksum success')
        #         return
        #
        if data[1] != "cs" and data!="hb":
            data=data[1]
            # print(data)
            if float(data[2]) > 0:
                global bids
                bids = {
                    str(data[0]): [str(data[1]), str(data[2])]
                }
                orderbooks[pair]['bids'] = bids
            elif float(data[2]) < 0:
                global asks
                asks = {
                    str(data[0]): [str(data[1]), str(data[2])[1:]]
                }
                orderbooks[pair]['asks'] = asks

        # Update orderbook
            # 1. - When count > 0 then you have to add or update the price level.
            #   1.1- If amount > 0 then add/update bids.
            #   1.2- If amount < 0 then add/update asks.
            # 2. - When count = 0 then you have to delete the price level.
            #   2.1- If amount = 1 then remove from bids
            #   2.2- If amount = -1 then remove from asks
            if int(data[1]) > 0:  # 1.
                  if float(data[2]) > 0:  # 1.1
                    orderbooks[pair]['bids'].update({str(data[0]): [str(data[1]), str(data[2])]})

                  elif float(data[2]) < 0:  # 1.2
                    orderbooks[pair]['asks'].update({str(data[0]): [str(data[1]), str(data[2])[1:]]})

            elif data[1] == '0':  # 2.
                if data[2] == '1':  # 2.1
                    if orderbooks[pair]['bids'].get(data[0]):
                        del orderbooks[pair]['bids'][data[0]]
                elif data[2] == '-1':  # 2.2
                    if orderbooks[pair]['asks'].get(data[0]):
                        del orderbooks[pair]['asks'][data[0]]
            # print(orderbooks) #dubug

async def printorderbook():
    global orderbooks
    while 1:
        await asyncio.sleep(10)
        for pair in PAIRS:
            bids = [[v[1], v[0], k] for k, v in orderbooks[pair]['bids'].items()]
            asks = [[k, v[0], v[1]] for k, v in orderbooks[pair]['asks'].items()]
            bids.sort(key=lambda x: float(x[2]), reverse=True)
            asks.sort(key=lambda x: float(x[0]))
            table = [[*bid, *ask] for (bid, ask) in zip(bids, asks)]
            headers = ['bid:amount', 'bid:count', 'bid:price', 'ask:price', 'ask:count', 'ask:amount']
            print('orderbook for {}'.format(pair))
            print(tabulate(table, headers=headers))


async def get_book(pair):
    print('enter get_book, pair: {}'.format(pair))
    pair_dict = deepcopy(conf)
    pair_dict.update({"symbol": pair})
    print(pair_dict)
    async with websockets.connect('wss://api-pub.bitfinex.com/ws/2') as websocket:
        await websocket.send('{ "event": "subscribe", "channel": "book", "symbol": "tBTCUSD"}')
        await websocket.send('{ "event": "conf", "flags": 131072 }')
        while True:
            if not websocket.open:
                print ('Websocket NOT connected. Trying to reconnect.')
                try:
                    websocket = await websockets.connect('wss://api-pub.bitfinex.com/ws/2')
                    await websocket.send('{ "event": "subscribe", "channel": "book", "symbol": "tBTCUSD"}')
                    await websocket.send('{ "event": "conf", "flags": 131072 }')
                except:
                    pass
            else:
                try:
                    global res
                    res = await websocket.recv()
                    # print(res)
                    await asyncio.sleep(1)
                except Exception as e:
                    print('Error!: ', e)
                    break
            # print(res)  # debug
            build_book(res,pair)



async def main():
    coroutine = [
            get_book(pair)
            for pair in PAIRS
        ]
    coroutine.append(printorderbook())
    await asyncio.wait(coroutine)

loop = asyncio.get_event_loop()
loop.run_until_complete(main())

