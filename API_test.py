import asyncio
import websockets
import json


async def test_ws_quote():
    async with websockets.connect('wss://api-pub.bitfinex.com/ws/2') as websocket:
        req1 = '{ "event": "conf", "flags": 131072 }'
        req2 = {'{ "event": "subscribe", "channel": "book", "symbol": "tBTCUSD"}'}
        await websocket.send(req1)
        await websocket.send(req2)

        while True:
            quote = await websocket.recv()
            print(quote)


asyncio.get_event_loop().run_until_complete(test_ws_quote())

#CheckSum
# as the returned data shows every book iteration, there will be a checkwum message : \
#     [ CHAIN_ID, 'cs', CHECKSUM ]
# where CHECKSUM is a signed integer.
# According to the API files, it uses CRC-32 checksum:
# So we can use CRC-32 to create the checksum value with the data returned and compare it with the checksum returned by the Websocket.
# But I was confused how we can get the checksum value by myself. Hope you can give me some tips.
